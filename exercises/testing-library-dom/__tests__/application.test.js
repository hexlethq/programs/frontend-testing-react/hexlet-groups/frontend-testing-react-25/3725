// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');

const run = require('../src/application');

const { screen } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

let app;

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();

  app = {
    listsContainer: document.querySelector('[data-container="lists"]'),
    tasksContainer: document.querySelector('[data-container="tasks"]'),
  };
});

// BEGIN
it('should show empty tasks list', () => {
  expect(screen.getByText('General')).toBeInTheDocument();
  expect(app.tasksContainer).toBeEmptyDOMElement();
});

it('should add task to default list', () => {
  const taskName = 'new task';
  const newTaskInput = screen.getByLabelText('New Task Name');
  
  userEvent.type(newTaskInput, taskName);
  userEvent.click(screen.getByRole('button', { name: /add task/i }));

  expect(newTaskInput).toHaveDisplayValue('');
  expect(screen.getByText(taskName)).toBeInTheDocument();
});

it('should add multiple tasks', () => {
  const tasks = ['first task', 'second task'];

  for (const task of tasks) {
    userEvent.type(screen.getByLabelText('New Task Name'), `${task}{enter}`);
    expect(screen.getByText(task)).toBeInTheDocument();
  }
});

it('should be able to add new list', () => {
  const listName = 'Special';
  
  userEvent.type(screen.getByLabelText('New Task Name'), 'new task{enter}');
  userEvent.type(screen.getByLabelText('New List Name'), `${listName}{enter}`);
  userEvent.click(screen.getByText(listName));

  expect(app.tasksContainer).toBeEmptyDOMElement();
});

it('should be able to add the task to the second list', () => {
  const listName = 'Special';
  const taskName = 'special task';
  
  userEvent.type(screen.getByLabelText('New List Name'), `${listName}{enter}`);
  userEvent.click(screen.getByText(listName));
  userEvent.type(screen.getByLabelText('New Task Name'), `${taskName}{enter}`);

  expect(screen.getByText(taskName)).toBeInTheDocument();
});
// END
