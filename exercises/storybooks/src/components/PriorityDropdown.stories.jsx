import React from 'react';

import PriorityDropdown from './PriorityDropdown';

// BEGIN
export default {
  component: PriorityDropdown,
  title: 'PriorityDropdown',
};

const Template = args => <PriorityDropdown {...args} />;

export const Default = Template.bind({});
Default.args = {
  variant: 'warning',
};

export const RightDirection = Template.bind({});
RightDirection.args = {
  ...Default.args,
  dropDirection: 'right',
};

export const SmallButton = Template.bind({});
SmallButton.args = {
  ...Default.args,
  size: 'sm',
};

export const LargeButton = Template.bind({});
LargeButton.args = {
  ...Default.args,
  size: 'lg',
};
// END
