const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('internal sort function', () => {
  it('should sort numbers', () => {
    fc.assert(
      fc.property(fc.float32Array(), (input) => {
        expect(sort(input)).toBeSorted();
      }),
    );
  });

  it('should sort booleans', () => {
    fc.assert(
      fc.property(fc.array(fc.boolean()), (input) => {
        expect(sort(input)).toBeSorted();
      }),
    );
  });

  it('should sort dates', () => {
    fc.assert(
      fc.property(fc.array(fc.date()), (input) => {
        expect(sort(input)).toBeSorted();
      }),
    );
  });

  it('should not change collection length', () => {
    fc.assert(
      fc.property(fc.float32Array(), (input) => {
        expect(sort(input).length).toBe(input.length);
      }),
    );
  });

  it('should not change the content', () => {
    fc.assert(
      fc.property(fc.float32Array(), (input) => {
        const sorted = sort(input);
        sorted.forEach((x) => expect(input).toContain(x));
      }),
    );
  });

  it('should be idempotent', () => {
    fc.assert(
      fc.property(fc.float32Array(), (input) => {
        const sorted = sort(input);
        expect(sort(sorted)).toEqual(sorted);
      }),
    );
  });
});

// END
