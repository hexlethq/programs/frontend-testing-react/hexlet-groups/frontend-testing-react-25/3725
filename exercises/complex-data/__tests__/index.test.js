const faker = require('faker');

// BEGIN
describe('faker.createTransaction()', () => {
  it('should generate unique values each time', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();

    expect(transaction1).not.toEqual(transaction2);
  });

  it('should have necessary properties', () => {
    const transaction = faker.helpers.createTransaction();

    expect(transaction).toEqual({
      amount: expect.any(String),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.any(String),
    });

    // check that can be casted to number
    expect(Number(transaction.account)).not.toBeNaN();
    expect(Number(transaction.amount)).not.toBeNaN();
  });
});
// END
