// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');

const run = require('../src/application');

let app;

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();

  app = {
    listsContainer: document.querySelector('[data-container="lists"]'),
    tasksContainer: document.querySelector('[data-container="tasks"]'),
    newListForm: document.querySelector('[data-container="new-list-form"]'),
    newTaskForm: document.querySelector('[data-container="new-task-form"]'),
  };
});

// BEGIN
it('should show empty list', () => {
  expect(app.listsContainer.querySelectorAll('li')).toHaveLength(1);
  expect(app.tasksContainer).toBeEmptyDOMElement();
});

it('should add task to default list', () => {
  const taskName = 'new task';
  app.newTaskForm.querySelector('[data-testid="add-task-input"]').value = taskName;

  const addTaskBtn = app.newTaskForm.querySelector('[data-testid="add-task-button"]');
  addTaskBtn.click();

  expect(app.newTaskForm).toHaveFormValues({
    name: '',
  });
  expect(app.tasksContainer).toHaveTextContent(taskName);
  expect(app.listsContainer).toHaveTextContent('General');
});

it('should add multiple tasks', () => {
  const tasks = ['first task', 'second task'];

  for (const task of tasks) {
    app.newTaskForm.querySelector('[data-testid="add-task-input"]').value = task;
    const addTaskBtn = app.newTaskForm.querySelector('[data-testid="add-task-button"]');
    addTaskBtn.click();
  }

  const taskNames = [...app.tasksContainer.querySelectorAll('li')].map(node => node.textContent);
  expect(taskNames).toEqual(tasks);
});

it('should be able to add new list', () => {
  app.newTaskForm.querySelector('[data-testid="add-task-input"]').value = 'new task';
  const addTaskBtn = app.newTaskForm.querySelector('[data-testid="add-task-button"]');
  addTaskBtn.click();

  app.newListForm.querySelector('[data-testid="add-list-input"]').value = 'Special';
  const addListBtn = app.newListForm.querySelector('[data-testid="add-list-button"]');
  addListBtn.click();

  app.listsContainer.querySelector('li a').click();

  expect(app.tasksContainer).toBeEmptyDOMElement();
});

it('should be able to add the task to the second list', () => {
  app.newListForm.querySelector('[data-testid="add-list-input"]').value = 'Special';
  const addListBtn = app.newListForm.querySelector('[data-testid="add-list-button"]');
  addListBtn.click();

  app.listsContainer.querySelector('li a').click();

  const taskName = 'special task';
  app.newTaskForm.querySelector('[data-testid="add-task-input"]').value = taskName;
  const addTaskBtn = app.newTaskForm.querySelector('[data-testid="add-task-button"]');
  addTaskBtn.click();

  expect(app.tasksContainer).toHaveTextContent(taskName);
});
// END
