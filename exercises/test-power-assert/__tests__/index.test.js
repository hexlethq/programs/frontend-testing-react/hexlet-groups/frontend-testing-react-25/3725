const assert = require('power-assert').customize({
  assertion: {
    saveContextOnRethrow: false,
  },
});

const { flattenDepth } = require('lodash');

// BEGIN
const array = [1, [2, [3, [4]], 5]];

// positive
assert.deepStrictEqual(flattenDepth(array), [1, 2, [3, [4]], 5]);
assert.deepStrictEqual(flattenDepth(array, 2), [1, 2, 3, [4], 5]);
assert.deepStrictEqual(flattenDepth(array, 3), [1, 2, 3, 4, 5]);
assert.deepStrictEqual(flattenDepth(array, 100), [1, 2, 3, 4, 5]);

// negative
assert.deepStrictEqual(flattenDepth(array, -1), array);
assert.deepStrictEqual(flattenDepth(array, 'incorrect'), array);
// END
