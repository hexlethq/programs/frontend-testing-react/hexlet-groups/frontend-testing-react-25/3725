/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */
/* eslint-disable no-return-assign */
// BEGIN
const port = 5000;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `${appUrl}/articles`;
const appNewArticleUrl = `${appArticlesUrl}/new`;

const newArticle = {
  name: 'Ayak Blog Post',
  category: 'optio quo quis',
  content: 'Post content',
};

describe('SimpleBlog app', () => {
  it('should open main page', async () => {
    await page.goto(appUrl);
    await expect(page).toMatch('Welcome to a Simple blog!');
  });

  it('should open all articles', async () => {
    await page.goto(appArticlesUrl);
    const articles = await page.$$('table tbody tr');
    expect(articles.length).toBeGreaterThan(1);
  });

  it('should open a form for the new article', async () => {
    await page.goto(appArticlesUrl);
    const link = await page.$('[data-testid="article-create-link"]');

    await Promise.all([
      link.click(),
      page.waitForNavigation(),
    ]);

    const newArticleForm = await page.$('form');
    expect(newArticleForm).toBeDefined();
  });

  it('should show the created article', async () => {
    await page.goto(appNewArticleUrl);

    await expect(page).toFillForm('form', {
      'article[name]': newArticle.name,
      'article[categoryId]': newArticle.category,
      'article[content]': newArticle.content,
    });

    await Promise.all([
      page.click('input[data-testid="article-create-button"]'),
      page.waitForNavigation(),
    ]);

    await expect(page).toMatch(newArticle.name);
  });

  it('should show the updated article', async () => {
    await page.goto(appArticlesUrl);
    const articles = await page.$$('table tbody tr');
    const editLink = await articles[0].$('a');

    await Promise.all([
      editLink.click(),
      page.waitForNavigation(),
    ]);

    await expect(page).toFillForm('form', {
      'article[name]': 'Updated article',
    });

    // await page.$eval('#name', (el) => el.value = 'Updated article');

    await Promise.all([
      page.click('input[data-testid="article-update-button"]'),
      page.waitForNavigation(),
    ]);

    await expect(page).toMatch('Updated article');
  });
});

// END
