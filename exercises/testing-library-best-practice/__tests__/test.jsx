// @ts-check

import React from 'react';
import nock from 'nock';
import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

import TodoBox from '../src/TodoBox.jsx';

axios.defaults.adapter = httpAdapter;
const host = 'http://localhost';

const tasks = [
  { id: '1', text: 'first', state: 'active' },
  { id: '2', text: 'second', state: 'active' },
  { id: '3', text: 'third', state: 'active' },
];

beforeAll(() => {
  nock.disableNetConnect();
});

afterAll(() => {
  nock.enableNetConnect();
});

// BEGIN
beforeEach(() => {
  tasks.forEach((task) => {
    nock(host)
      .post('/tasks', { text: task.text })
      .reply(200, task);
  });

  nock(host)
    .get('/tasks')
    .reply(200, [])
    .patch('/tasks/2/finish')
    .reply(200, { id: '2', text: 'second', state: 'finished' })
    .patch('/tasks/2/activate')
    .reply(200, { id: '2', text: 'second', state: 'active' });
});

it('should have an empty state', () => {
  render(<TodoBox />);

  expect(screen.queryByRole('link')).not.toBeInTheDocument();
});

it('should create tasks, finish them and activate back', async () => {
  const { asFragment } = render(<TodoBox />);

  // Add tasks
  for (const task of tasks) {
    userEvent.type(screen.getByRole('textbox'), task.text);
    userEvent.click(screen.getByRole('button', { name: /add/i }));

    await screen.findByRole('link', { name: task.text });
  }

  // Change task state to finish and activate back 
  const taskToClick = 'second';
  for (const _performAction of ['finish task', 'activate task']) {
    const taskLink = screen.getByText(taskToClick);
    userEvent.click(taskLink);
  }
  expect(asFragment()).toMatchSnapshot();
});
// END
 