// @ts-check

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const host = 'http://localhost';

const replies = {
  'b': ["Bangladesh", "Belgium", "Burkina Faso", "Bulgaria", "Bahrain", "Burundi"],
  'be': ["Belgium", "Benin", "Bermuda", "Belarus", "Belize"],
  'bel': ["Belgium", "Belarus", "Belize"],
  'bela': ["Belarus"],
}

beforeAll(() => {
  nock.disableNetConnect();
});

beforeEach(() => {
  Object.entries(replies).forEach(([term, response]) => {
    nock(`${host}`)
      .get(`/countries?term=${term}`)
      .reply(200, response)
  });
});

// BEGIN
test.each([
  'b',
  'be',
  'bel',
  'bela'
])('should autocomplete countries by: %s', async (letters) => {
  render(<Autocomplete />);

  userEvent.type(screen.getByRole('textbox'), letters);
  await waitFor(() => screen.getByRole('list'));

  const countries = [...screen.getAllByRole('listitem')].map(node => node.textContent);
  expect(countries).toEqual(replies[letters]);

  userEvent.clear(screen.getByRole('textbox'));
  expect(screen.queryByRole('list')).not.toBeInTheDocument();
});
// END
