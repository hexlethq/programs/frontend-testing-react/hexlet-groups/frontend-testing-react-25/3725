/* eslint-disable no-undef */
// BEGIN
// @ts-check
import SimpleTodoPage from '../models/simpleTodo';

describe('Simple Todo', () => {
  it('should open', async () => {
    const page = await browser.newPage();
    const simpleTodoPage = new SimpleTodoPage(page);

    await simpleTodoPage.navigate();
    expect(simpleTodoPage.textBox()).toBeDefined();
    expect(simpleTodoPage.addButton()).toBeDefined();
  });

  it('should add new task', async () => {
    const page = await browser.newPage();
    const simpleTodoPage = new SimpleTodoPage(page);

    await simpleTodoPage.navigate();
    await simpleTodoPage.addTask('my task');
    await expect(page).toMatch('my task');
  });

  it('should allow to delete task', async () => {
    const page = await browser.newPage();
    const simpleTodoPage = new SimpleTodoPage(page);

    await simpleTodoPage.navigate();
    await simpleTodoPage.addTask('task 1');
    await simpleTodoPage.addTask('task 2');
    await simpleTodoPage.addTask('task 3');

    await simpleTodoPage.deleteTaskById(2);
    await expect(page).not.toMatch('task 2');
  });
});
// END
