/* eslint-disable no-undef */
import { rest } from 'msw';
import getNextId from '../lib/getNextId';

// eslint-disable-next-line import/prefer-default-export
export const handlers = [
  rest.post('/tasks', (req, res, ctx) => {
    const taskId = getNextId();
    const task = {
      id: taskId.toString(),
      text: req.body.task.text,
      state: 'active',
    };
    sessionStorage.setItem(taskId.toString(), JSON.stringify(task));

    return res(
      ctx.status(200),
      ctx.json(task),
    );
  }),

  rest.get('/tasks/:taskId', (req, res, ctx) => {
    const { taskId } = req.params;

    const task = sessionStorage.getItem(taskId);
    return res(
      ctx.status(200),
      ctx.json(JSON.parse(task)),
    );
  }),

  rest.get('/tasks', (req, res, ctx) => res(
    ctx.status(200),
    ctx.json(Object.values(sessionStorage)
      .map((o) => JSON.parse(o))
      .sort((a, b) => b.id - a.id)),
  )),

  rest.delete('/tasks/:taskId', (req, res, ctx) => {
    const { taskId } = req.params;

    sessionStorage.removeItem(taskId);
    return res(
      ctx.status(204),
    );
  }),
];
