const getHandleBy = async (page, selector) => {
  await page.waitForSelector(selector);

  return page.$(selector);
};

export default class SimpleTodoPage {
  constructor(page) {
    this.page = page;
  }

  async navigate() {
    await this.page.goto('http://localhost:8080');
  }

  async textBox() {
    return getHandleBy(this.page, '[data-testid="task-name-input"]');
  }

  async addButton() {
    return getHandleBy(this.page, '[data-testid="add-task-button"]');
  }

  async deleteButton(id) {
    return getHandleBy(this.page, `[data-testid="remove-task-${id}"]`);
  }

  async addTask(text) {
    const elementHandle = await this.textBox();
    await elementHandle.type(text, { delay: 25 });
    await this.page.keyboard.press('Enter');
  }

  async deleteTaskById(id) {
    const elementHandle = await this.deleteButton(id);
    await elementHandle.click();
  }
}
