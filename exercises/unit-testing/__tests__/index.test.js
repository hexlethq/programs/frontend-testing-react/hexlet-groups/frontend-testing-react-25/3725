test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();

  // BEGIN
  expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
  expect(result).toBe(target);
  // END
});

test('error for read-only property', () => {
  const f = () => {
    const target = Object.defineProperty({}, 'prop', {
      value: 'read',
      writable: false,
    });
    const src = { prop: 'write' };
    Object.assign(target, src);
  };

  expect(f).toThrow(TypeError);
});
