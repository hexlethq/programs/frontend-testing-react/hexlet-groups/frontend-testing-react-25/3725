const { iteratee, filter } = require('lodash');
const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;
const appNewArticleUrl = `${appArticlesUrl}/new`;

const newArticle = {
  name: 'Ayak Blog Post',
  category: '1',
  content: 'Post content',
};

let browser;
let page;

const app = getApp();

describe('simple blog', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-gpu'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  it('should open main page', async () => {
    await page.goto(appUrl);
    const element = await page.$('#title');
    const text = await page.evaluate(element => element.textContent, element);
    expect(text).toEqual('Welcome to a Simple blog!');
  });

  it('should open all articles', async () => {
    await page.goto(appArticlesUrl);
    const articles = await page.$$('#articles tbody tr');
    expect(articles).toHaveLength(4);
  });

  it('should open a form for the new article', async () => {
    await page.goto(appArticlesUrl);
    const links = await page.$$('div a');

    const link = filter(links, async (link) => {
      const attr = await page.evaluate(el => el.getAttribute('href'), link);
      return attr === appNewArticleUrl;
    })[0];
    await Promise.all([
      link.click(),
      page.waitForNavigation(),
    ]);

    const newArticleForm = await page.$('form');
    expect(newArticleForm).toBeDefined();
  });

  it('should create new article', async () => {
    await page.goto(appNewArticleUrl);
    const newArticleForm = await page.$('form');

    await Promise.all([
      newArticleForm.$eval('#name', (el, value) => el.value = value, newArticle.name),
      newArticleForm.$eval('#category', (el, value) => el.value = value, newArticle.category),
      newArticleForm.$eval('#content', (el, value) => el.value = value, newArticle.content)
    ]);

    await Promise.all([
      page.click('input[type="submit"]'),
      page.waitForNavigation(),
    ]);
  });

  it('should show the created article', async () => {
    await page.goto(appNewArticleUrl);
    const newArticleForm = await page.$('form');

    await Promise.all([
      newArticleForm.$eval('#name', (el, value) => el.value = value, newArticle.name),
      newArticleForm.$eval('#category', (el, value) => el.value = value, newArticle.category),
      newArticleForm.$eval('#content', (el, value) => el.value = value, newArticle.content)
    ]);

    await Promise.all([
      page.click('input[type="submit"]'),
      page.waitForNavigation(),
    ]);

    const bodyHandle = await page.$('html');
    const bodyContent = await bodyHandle.$eval('body', (el) => el.innerHTML);
    expect(bodyContent).toEqual(expect.stringContaining(newArticle.name));
  });

  it('should show the updated article', async () => {
    await page.goto(appArticlesUrl);
    let articles = await page.$$('#articles tbody tr');
    const editLink = await articles[0].$('a');

    await Promise.all([
      editLink.click(),
      page.waitForNavigation(),
    ]);

    await page.$eval('#name', el => el.value = 'Updated article');

    await Promise.all([
      page.click('input[type="submit"]'),
      page.waitForNavigation(),
    ]);

    const bodyHandle = await page.$('html');
    const bodyContent = await bodyHandle.$eval('body', (el) => el.innerHTML);
    expect(bodyContent).toEqual(expect.stringContaining('Updated article'));
  });
  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
