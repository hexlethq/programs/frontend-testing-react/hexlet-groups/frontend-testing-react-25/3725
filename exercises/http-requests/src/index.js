const axios = require('axios');

// BEGIN
const get = async (url) => {
  const response = await axios.get(url);
  return response.data;
};

const post = (url, payload) => axios.post(url, JSON.stringify(payload));
// END

module.exports = { get, post };
