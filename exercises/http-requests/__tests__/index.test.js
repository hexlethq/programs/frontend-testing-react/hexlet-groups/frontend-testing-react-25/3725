const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN

const user = {
  firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33,
};

describe('/users', () => {
  beforeEach(() => {
    nock.disableNetConnect();
  });

  afterEach(() => {
    nock.cleanAll();
    nock.enableNetConnect();
  });

  it('should support CRUD operations', async () => {
    nock('http://example.com')
      .get('/users')
      .reply(200, '[]', {
        'Content-Type': 'application/json',
      })
      .post('/users')
      .reply(200)
      .get('/users')
      .reply(200, JSON.stringify([user]), {
        'Content-Type': 'application/json',
      });

    const usersBefore = await get('http://example.com/users');
    expect(usersBefore).toEqual([]);

    const res = await post('http://example.com/users', user);
    expect(res.status).toEqual(200);
    const usersAfter = await get('http://example.com/users');
    expect(usersAfter[0]).toEqual(user);

    expect(nock.isDone()).toEqual(true);
  });

  test.each([
    [400],
    [500],
  ])('should return %d error', async (expectedCode) => {
    nock('http://example.com')
      .post('/users')
      .reply(expectedCode);

    await expect(post('http://example.com/users', {})).rejects.toThrow(
      `Request failed with status code ${expectedCode}`,
    );
    expect(nock.isDone()).toEqual(true);
  });
});
// END
